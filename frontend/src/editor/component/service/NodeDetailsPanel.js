import React from 'react';
import {connect} from "react-redux";
import ServiceField from "./ServiceField";
import ServiceLayer from "./ServiceLayer";
import ServiceName from "./ServiceName";

/* eslint jsx-a11y/alt-text: 0*/

const mapDispatchToProps = (dispatch) => {
    return {}
};
let mapStateToProps = (state) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(
    () => {
        return (
            <div className="row form-horizontal dark_well serviceLayerPanel">
                <div className="row">&nbsp;</div>

                <div className="col-lg-12">
                    <ul className="nav nav-tabs" role="tablist" id="node-services-tab-list">
                        <li role="presentation" className="active">
                            <a aria-controls="home" role="tab" data-toggle="tab">OS</a>
                        </li>
                    </ul>
                    <br/>
                    <div className="tab-content" id="node-services-tab-content">
                        <div className="tab-pane active" id="svc_data_tab_0">
                            <ServiceName name="Node OS"/>
                            <ServiceLayer layer={0} layourCount={1}/>
                            <ServiceField type="small" name="Node id"/>
                            <ServiceField type="small" name="Network ▣"/>
                            <ServiceField type="large" name="Node name"/>
                            <ServiceField type="large" name="SL Info"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    });
