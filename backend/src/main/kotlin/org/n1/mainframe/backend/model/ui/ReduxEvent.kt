package org.n1.mainframe.backend.model.ui

class ReduxEvent(val type: String,
                 val data: Any? = null)
