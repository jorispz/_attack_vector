package org.n1.mainframe.backend.model.ui

data class SiteCommand(
        val siteId: String = ""
)