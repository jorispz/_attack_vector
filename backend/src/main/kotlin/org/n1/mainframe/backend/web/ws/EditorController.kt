package org.n1.mainframe.backend.web.ws

import com.google.gson.Gson
import mu.KLogging
import org.n1.mainframe.backend.model.ui.*
import org.n1.mainframe.backend.service.*
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.stereotype.Controller
import java.security.Principal
import org.springframework.messaging.simp.annotation.SendToUser
import org.springframework.messaging.handler.annotation.MessageExceptionHandler


@Controller
class EditorController(
        val editorService: EditorService
) {

    companion object : KLogging()

    private val gson = Gson()

    @MessageMapping("/siteFull")
    fun siteFull(siteId: String) {
        logger.info { "sendSiteFull: ${siteId}" }
        editorService.sendSiteFull(siteId)
    }

    @MessageMapping("/addNode")
    fun addNode(command: AddNode) {
        val json = gson.toJson(command)
        logger.info { "addNode: ${json}" }
        editorService.addNode(command)
    }

    @MessageMapping("/moveNode")
    fun moveNode(command: MoveNode) {
        val json = gson.toJson(command)
        logger.info { "moveNode: ${json}" }
        editorService.moveNode(command)
    }

    @MessageMapping("/addConnection")
    fun addConnection(command: AddConnection) {
        val json = gson.toJson(command)
        logger.info { "addConnection: ${json}" }
        editorService.addConnection(command)
    }

    @MessageMapping("/editSiteData")
    fun editSiteData(command: EditSiteData, principal: Principal) {
        val json = gson.toJson(command)
        logger.info { "update: ${json}" }
        editorService.update(command, principal)
    }

    @MessageMapping("/deleteConnections")
    fun deleteConnections(command: DeleteNodeCommand) {
        val json = gson.toJson(command)
        logger.info { "deleteConnections: ${json}" }
        editorService.deleteConnections(command)
    }

    @MessageMapping("/deleteNode")
    fun deleteNode(command: DeleteNodeCommand) {
        val json = gson.toJson(command)
        logger.info { "deleteNode: ${json}" }
        editorService.deleteNode(command)
    }

    @MessageMapping("/snap")
    fun snap(command: SiteCommand) {
        val json = gson.toJson(command)
        logger.info { "snap: ${json}" }
        editorService.snap(command)
    }

    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

    @MessageExceptionHandler
    @SendToUser("/error")
    fun handleException(exception: Exception): NotyMessage {
        if (exception is ValidationException) {
            return exception.getNoty()
        }
        logger.error(exception.message, exception)
        return NotyMessage("fatal", "Server error", exception.message ?: "" )
    }
}