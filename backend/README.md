
              _|_|      _|      _|                          _|            _|      _|                        _|
            _|    _|  _|_|_|_|_|_|_|_|    _|_|_|    _|_|_|  _|  _|        _|      _|    _|_|      _|_|_|  _|_|_|_|    _|_|    _|  _|_|
            _|_|_|_|    _|      _|      _|    _|  _|        _|_|          _|      _|  _|_|_|_|  _|          _|      _|    _|  _|_|
            _|    _|    _|      _|      _|    _|  _|        _|  _|          _|  _|    _|        _|          _|      _|    _|  _|
            _|    _|      _|_|    _|_|    _|_|_|    _|_|_|  _|    _|          _|        _|_|_|    _|_|_|      _|_|    _|_|    _|
    _|_|_|

A hacking simulation intended for use in Live Action RolePlaying games.


##Running:

The application is a Spring boot standalone java app, the main method class is: org.n1.mainframe.backend.AttackVector

Add environment variables for configuration:

`MONGODB_URI`     The connect URL for Mongo DB. For example: mongodb://av2:****@localhost/admin?authMechanism=SCRAM-SHA-1

`ENVIRONMENT`     The name of the server environment. For example: dev-ervi [1]

`MONGODB_NAME`    The name of the database. Optional, if not set, then the database from the MONGODB_URI is used. In this example: av2


[1] This is used, for example to simulate slower server-like response times in local development. (see StompService)


##Mongo on docker

once you have installed a docker image, create the container with:

  `docker run -p27017:27017 --name mongo mongo`


Next time run command

  `docker start mongo`


##Create mongo user

Creating a user: av2 with password av2:

Step 1. log into a running mongo docker with:

  `docker exec -i -t mongo bash`

  (or if you are running from git bash on windows: `winpty docker exec -i -t mongo bash` )

Step 2. start mongo client with

  `mongo`

Step 3. create user

  `use admin`

  `db.createUser( { user: "av2",
                   pwd: "av2",
                   roles: [ "readWrite"] },
                 { } )`



Step 4. mongo URL:

with this user on a local host mongo: the environment variables become:

MONGODB_URI = mongodb://av2:av2@localhost/admin?authMechanism=SCRAM-SHA-1
MONGODB_NAME = av2
ENVIRONMENT = dev-username





##Deploy to Herouku:

> optional, if you want to do Heroku stuff (does not work in windows git-bash)
heroku login

> upload, build & deploy:
git push heroku master
